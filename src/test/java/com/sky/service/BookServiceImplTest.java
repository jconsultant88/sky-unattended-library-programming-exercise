package com.sky.service;

import com.sky.library.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BookServiceImplTest {

    private static final String BOOK_REFERENCE_PREFIX = "BOOK-";
    private static final String THE_GRUFFALO_REFERENCE = BOOK_REFERENCE_PREFIX + "GRUFF472";
    private static final String THE_WIND_IN_THE_WILLOWS_REFERENCE = BOOK_REFERENCE_PREFIX + "WILL987";
    private static final String INVALID_BOOK_REF = BOOK_REFERENCE_PREFIX + "999";

    private BookService bookService;
    private final BookRepository bookRepository = new BookRepositoryStub();

    @Before
    public void setup() {
        bookService = new BookServiceImpl(bookRepository);
    }

    @Test(expected = IllegalArgumentException.class)
    public void retrieveBookOnInvalidBookReference() throws BookNotFoundException {
        bookService.retrieveBook("INVALID-TEXT");
    }

    @Test
    public void retrieveBookThrowsBookNotFoundException() {
        //When
        try {
            bookService.retrieveBook(INVALID_BOOK_REF);
            fail("should not reached this point");
        } catch (BookNotFoundException e) {
            //  Then
            assertNull(e.getMessage());
        }
    }

    @Test
    public void retrieveBookOnSuccess() throws BookNotFoundException {
        //When
        final Book book = bookService.retrieveBook(THE_GRUFFALO_REFERENCE);

        //Then
        assertNotNull(book);
        assertEquals("The Gruffalo", book.getTitle());
        assertEquals("BOOK-GRUFF472", book.getReference());
        assertEquals("A mouse taking a walk in the woods", book.getReview());
    }

    @Test
    public void getBookSummaryOnInvalidBookReference() throws BookNotFoundException {
        try {
            //When
            bookService.getBookSummary("INVALID-TEXT");
            fail("should not reached this point");
        } catch (IllegalArgumentException e) {
            //  Then
            assertEquals("Book reference must begin with " + BOOK_REFERENCE_PREFIX, e.getMessage());
        }
    }

    @Test
    public void getBookSummaryThrowsBookNotFoundException() {
        //When
        try {
            bookService.getBookSummary(INVALID_BOOK_REF);
            fail("should not reached this point");
        } catch (BookNotFoundException e) {
            //  Then
            assertNull(e.getMessage());
        }
    }

    @Test
    public void getBookSummaryOnSuccess() throws BookNotFoundException {
        //When
        final String summary = bookService.getBookSummary(THE_WIND_IN_THE_WILLOWS_REFERENCE);

        //Then
        assertNotNull(summary);
        assertEquals("[BOOK-WILL987] The Wind In The Willows - With the arrival of spring and fine weather outside...", summary);
    }

}
