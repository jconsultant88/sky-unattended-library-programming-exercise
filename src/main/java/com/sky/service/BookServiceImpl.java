package com.sky.service;

import com.sky.library.Book;
import com.sky.library.BookNotFoundException;
import com.sky.library.BookRepository;
import com.sky.library.BookService;
import com.sky.util.MiscUtils;

import java.util.StringJoiner;

import static java.util.Optional.*;

public class BookServiceImpl implements BookService {

    private static final String BOOK_REFERENCE_PREFIX = "BOOK-";
    private static final int BOOK_REVIEW_MAX_WORDS = 9;
    private static final String ELLIPSIS = "...";

    private BookRepository bookRepository;

    public BookServiceImpl(final BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public Book retrieveBook(String bookReference) throws BookNotFoundException {
        ofNullable(bookReference)
                .filter(bookRef -> bookRef.trim().startsWith(BOOK_REFERENCE_PREFIX))
                .orElseThrow(() -> new IllegalArgumentException("Book reference must begin with " + BOOK_REFERENCE_PREFIX));

        Book book = bookRepository.retrieveBook(bookReference);

        return ofNullable(book)
                .orElseThrow(() -> new BookNotFoundException());
    }

    @Override
    public String getBookSummary(String bookReference) throws BookNotFoundException {
        Book book = retrieveBook(bookReference);
        return constructSummary(book);
    }

    private String constructSummary(final Book book) {
        StringJoiner stringJoiner = new StringJoiner(book.getReference(), "[", "]");
        stringJoiner.add(" ");
        String bookReview = MiscUtils.trimStringTo(book.getReview(), BOOK_REVIEW_MAX_WORDS, ELLIPSIS);
        bookReview = makeReviewReadable(bookReview);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        stringBuilder.append(book.getReference());
        stringBuilder.append("] ");
        stringBuilder.append(book.getTitle());
        stringBuilder.append(" - ");
        stringBuilder.append(bookReview);
        return stringBuilder.toString();
    }

    private String makeReviewReadable(String review) {
        return ofNullable(review)
                .filter(str -> str.endsWith("," + ELLIPSIS))
                .map(str -> str.replace("," + ELLIPSIS, ELLIPSIS))
                .orElse(review);
    }
}
